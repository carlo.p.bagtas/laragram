
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" style="text/css" href="../../css/cover.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <title>Instagram Photos and Videos Downloader</title>
</head>
<body class="text-center">
    <canvas id="canvas-basic"></canvas>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand text-light">Laragram</h3>
        </div>
      </header>
      <main role="main" class="inner cover text-light">
        <h1 class="cover-heading">Instagram photo and video downloader online.</h1>
        <p class="lead">To download a photo or video from Instagram, copy its URL, paste in the URL field above, and click "Download". You will get the photo or video download button to save the file to your computer.</p>
         <form action="" method="post">
    {{ csrf_field() }}
    <div class="col-lg-12">
       <p class="lead">
        <div class="input-group">
          <input class="form-control" type="text" placeholder="Instagram Link" name="url" required>
          <div class="input-group-append">
            <button class="btn btn-xs btn-secondary">Submit</button>
          </div>
        </div>

      </p>
    </div>
        <p class="lead">
        </p>
    </form>
      </main>

      <footer class="mastfoot mt-auto text-light">
        <div class="inner">
          <p>@Laragram 2018</p>
        </div>
      </footer>
    </div>
   
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/granim/1.1.1/granim.min.js"></script>
    <script>
      var granimInstance = new Granim({
          element: '#canvas-basic',
          name: 'basic-gradient',
          direction: 'left-right', // 'diagonal', 'top-bottom', 'radial'
          opacity: [1, 1],
          isPausedWhenNotInView: true,
          states : {
              "default-state": {
                  gradients: [
                      ['#AA076B', '#61045F'],
                      ['#02AAB0', '#00CDAC'],
                      ['#DA22FF', '#9733EE']
                  ]
              }
          }
      });
    </script>
</body>
</html>