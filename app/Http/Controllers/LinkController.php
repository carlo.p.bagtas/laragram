<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Opengraph;
use Illuminate\Support\Str;
use App\Link;

class LinkController extends Controller
{
    //

    public function grab(Request $request){
        
        $reader = new Opengraph\Reader();
        $reader->parse(file_get_contents($request->url));
        $types = [
            "instapp:photo" => "og:image.0.og:image:url",
            "video" => "og:video.0.og:video:secure_url",
        ];
        $meta = array_dot($reader->getArrayCopy());
        $url = $meta[$types[$meta["og:type"]]];
        $info = pathinfo($url);

        $link = new Link([
            "link"=>$url,
            "name"=>str_slug($meta["og:title"]).".".$info["extension"],
        ]);
        $link->save();
        $id = encrypt($link->id);
        try{
            return redirect("/".$id);
        }
        catch(\Exception $e){
            // dd($e);
        }
    }

    public function download($id){
        $file = Link::find(decrypt($id));
        $data = file_get_contents($file->link);
        
        return response()->streamDownload(function() use($data){
            echo $data;
        },e($file->name));
    }
}
